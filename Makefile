default: exit maximum power factorial toupper write-records read-records add-year add-year-werror

%.o: %.s
	as --gen-debug -c -o $@ $<

exit: exit.o
	ld exit.o -o exit

maximum: maximum.o
	ld maximum.o -o maximum

power: power.o
	ld power.o -o power

factorial: factorial.o
	ld factorial.o -o factorial

toupper: toupper.o
	ld toupper.o -o toupper

write-records: write-records.o write-record.o
	ld write-record.o write-records.o -o write-records

read-records: read-records.o read-record.o count-chars.o write-newline.o
	ld read-records.o read-record.o count-chars.o write-newline.o -o read-records

add-year: add-year.o write-record.o read-record.o
	ld add-year.o write-record.o read-record.o -o add-year

add-year-werror: add-year-werror.o write-record.o read-record.o error-exit.o count-chars.o write-newline.o
	ld add-year-werror.o write-record.o read-record.o error-exit.o count-chars.o write-newline.o -o add-year-werror

clean:
	-rm *.o exit maximum power factorial toupper write-records read-records add-year add-year-werror test.dat
