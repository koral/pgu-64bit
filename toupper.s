#PURPOSE:    This program converts an input file to an output file with all
#            letters converted to uppercase.
#
#PROCESSING: 1) Open the input file
#            2) Open the output file
#            4) While we're not at the end of the input file
#               a) read part of the file into our piece of memory
#               b) go through each byte of memory
#                    if the byte is a lower-case letter, convert it to uppercase
#               c) write the piece of memory to the output file

	.section .data  #we actually don't put anything in the data section in
	                #this program, but it's here for completeness

#######CONSTANTS########

	#system call numbers
        #http://blog.rchapman.org/post/36801038863/linux-system-call-table-for-x86-64
	.equ SYS_OPEN, 2
	.equ SYS_WRITE, 1
	.equ SYS_READ, 0
	.equ SYS_CLOSE, 3
	.equ SYS_EXIT, 60

	#options for open   (look at /usr/include/asm/fcntl.h for
	#                    various values.  You can combine them
	#                    by adding them)
	.equ O_RDONLY, 0                  #Open file options - read-only
	.equ O_CREAT_WRONLY_TRUNC, 03101  #Open file options - these options are:
	                                  #CREAT - create file if it doesn't exist
	                                  #WRONLY - we will only write to this file
	                                  #TRUNC - destroy current file contents, if any exist

	#end-of-file result status
	.equ END_OF_FILE, 0  #This is the return value of read() which
	                     #means we've hit the end of the file

#######BUFFERS#########

.section .bss
	#This is where the data is loaded into from
	#the data file and written from into the output file.  This
	#should never exceed 16,000 for various reasons.
	.equ BUFFER_SIZE, 500
	.lcomm BUFFER_DATA, BUFFER_SIZE


#######PROGRAM CODE###

	.section .text

	#STACK POSITIONS
	.equ ST_SIZE_RESERVE, 16
	.equ ST_FD_IN, 0
	.equ ST_FD_OUT, 8
        #vd. linux ABI pag. 29 and add ST_SIZE_RESERVE
	.equ ST_ARGC, 16      #Number of arguments
	.equ ST_ARGV_0, 24   #Name of program
	.equ ST_ARGV_1, 32   #Input file name
	.equ ST_ARGV_2, 40   #Output file name

	.globl _start
_start:
	###INITIALIZE PROGRAM###
	subq  $ST_SIZE_RESERVE, %rsp       #Allocate space for our pointers on the stack
	movq  %rsp, %rbp

open_files:
open_fd_in:
	###OPEN INPUT FILE###
	movq  ST_ARGV_1(%rbp), %rdi  #input filename into %rdi
	movq  $O_RDONLY, %rsi        #read-only flag
	movq  $0666, %rdx            #this doesn't really matter for reading
	movq  $SYS_OPEN, %rax        #open syscall
	syscall                      #call Linux

store_fd_in:
	movq  %rax, ST_FD_IN(%rbp)   #save the given file descriptor

open_fd_out:
	###OPEN OUTPUT FILE###
	movq  ST_ARGV_2(%rbp), %rdi        #output filename into %rdi
	movq  $O_CREAT_WRONLY_TRUNC, %rsi  #flags for writing to the file
	movq  $0666, %rdx                  #permission set for new file (if it's created)
	movq  $SYS_OPEN, %rax              #open the file
        syscall                            #call Linux

store_fd_out:
	movq  %rax, ST_FD_OUT(%rbp)        #store the file descriptor here

	###BEGIN MAIN LOOP###
read_loop_begin:

	###READ IN A BLOCK FROM THE INPUT FILE###
	movq  ST_FD_IN(%rbp), %rdi     #get the input file descriptor
	movq  $BUFFER_DATA, %rsi       #the location to read into
	movq  $BUFFER_SIZE, %rdx       #the size of the buffer
	movq  $SYS_READ, %rax
        syscall                        #call Linux
	                               #Size of buffer read is
	                               #returned in %eax

	###EXIT IF WE'VE REACHED THE END###
	cmpl  $END_OF_FILE, %eax       #check for end of file marker
	jle   end_loop                 #if found, go to the end

continue_read_loop:
	###CONVERT THE BLOCK TO UPPER CASE###
	pushq $BUFFER_DATA             #location of the buffer
	pushq %rax                     #size of the buffer
	call  convert_to_upper
	popq  %rax
	popq  %rbx

	###WRITE THE BLOCK OUT TO THE OUTPUT FILE###
	movq  ST_FD_OUT(%rbp), %rdi    #file to use
	movq  $BUFFER_DATA, %rsi       #location of the buffer
	movq  %rax, %rdx               #size of the buffer
	movq  $SYS_WRITE, %rax
        syscall                        #call Linux

	###CONTINUE THE LOOP###
	jmp   read_loop_begin

end_loop:
	###CLOSE THE FILES###
	#NOTE - we don't need to do error checking on these, because
	#       error conditions don't signify anything special here
	movq  ST_FD_OUT(%rbp), %rdi
	movq  $SYS_CLOSE, %rax
        syscall                      #call Linux

	movq  ST_FD_IN(%rbp), %rdi
	movq  $SYS_CLOSE, %rax
        syscall                      #call Linux

	###EXIT###
	movq  $0, %rdi
	movq  $SYS_EXIT, %rax
        syscall

#####FUNCTION convert_to_upper
#
#PURPOSE:   This function actually does the conversion to upper case for a block
#
#INPUT:     The first parameter is the length of that buffer
#           The second parameter is the location of the block of memory to convert
#
#OUTPUT:    This function overwrites the current buffer with the upper-casified
#           version.
#
#VARIABLES:
#           %eax - beginning of buffer
#           %ebx - length of buffer
#           %edi - current buffer offset
#           %cl - current byte being examined (%cl is the first byte of %ecx)
#

	###CONSTANTS##
	.equ  LOWERCASE_A, 'a'              #The lower boundary of our search
	.equ  LOWERCASE_Z, 'z'              #The upper boundary of our search
	.equ  UPPER_CONVERSION, 'A' - 'a'   #Conversion between upper and lower case

	###STACK POSITIONS###
	.equ  ST_BUFFER_LEN, 16              #Length of buffer
	.equ  ST_BUFFER, 24                  #actual buffer
convert_to_upper:
	pushq %rbp
	movq  %rsp, %rbp

	###SET UP VARIABLES###
	movq  ST_BUFFER(%rbp), %rax
	movq  ST_BUFFER_LEN(%rbp), %rbx
	movq  $0, %rdi

	#if a buffer with zero length was given us, just leave
	cmpq  $0, %rbx
	je    end_convert_loop

convert_loop:
	#get the current byte
	movb  (%rax,%rdi,1), %cl

	#go to the next byte unless it is between 'a' and 'z'
	cmpb  $LOWERCASE_A, %cl
	jl    next_byte
	cmpb  $LOWERCASE_Z, %cl
	jg    next_byte

	#otherwise convert the byte to uppercase
	addb  $UPPER_CONVERSION, %cl
	#and store it back
	movb  %cl, (%rax,%rdi,1)
next_byte:
	incq  %rdi              #next byte
	cmpq  %rdi, %rbx        #continue unless we've reached the end
	jne   convert_loop

end_convert_loop:
	#no return value, just leave
	movq  %rbp, %rsp
	popq  %rbp
	ret
